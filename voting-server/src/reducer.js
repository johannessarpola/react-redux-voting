/**
 * Created by Johannes on 25.3.2017.
 */
import {setEntries, nextTwo, vote, INITIAL_STATE} from './core';

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'SET_ENTRIES':
            return setEntries(state, action.entries);
        case 'NEXT':
            return nextTwo(state);
        case 'VOTE':
            // Take only part of the state
            return state.update('vote',
                voteState => vote(voteState, action.entry));
    }
    return state;
}