import {expect} from 'chai';
import {describe} from "mocha";
import {List, Map} from 'immutable';

describe('immutability', () => {

    describe('a number', () => {

        function increment(currentState) {
            return currentState + 1;
        }
        it('is immutable', () => {
            let state = 42;
            let nextState = increment(state);
            expect(nextState).to.equal(43);
            expect(state).to.equal(42);
        });

    });
    describe('A List', () => {

        function addMovie(currentState, movie) {
            return currentState.push(movie);
        }
        // Alright so it appears to use deep cloning in which existing
        // lists can't change but new ones are always created with change
        it('list should be immutable', () => {
            let state = List.of('Trainspotting', '28 Days Later');
            let nextState = addMovie(state, 'Sunshine');

            expect(nextState).to.equal(List.of(
                'Trainspotting',
                '28 Days Later',
                'Sunshine'
            ));
            expect(state).to.equal(List.of(
                'Trainspotting',
                '28 Days Later'
            ));
        });

    });
    describe('a tree', () => {

        function addMovieWithHelper(currentState, movie){
            return currentState.update('movies', movies => movies.push(movie));
        }
        function addMovie(currentState, movie) {
            return currentState.set(
                'movies',
                currentState.get('movies').push(movie)
            );
        }

        it('object tree should be immutable', () => {
            let state = Map({
                movies: List.of('Trainspotting', '28 Days Later')
            });
            let nextState = addMovie(state, 'Sunshine');
            let thirdState = addMovieWithHelper(state, '12 Monkeys')
            expect(nextState).to.equal(Map({
                movies: List.of(
                    'Trainspotting',
                    '28 Days Later',
                    'Sunshine'
                )
            }));
            expect(state).to.equal(Map({
                movies: List.of(
                    'Trainspotting',
                    '28 Days Later'
                )
            }));

            expect(thirdState).to.equal(Map({
                movies: List.of(
                    'Trainspotting',
                    '28 Days Later',
                    '12 Monkeys'
                )
            }));
        });

    });
});